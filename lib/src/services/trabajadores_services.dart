import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:wynservices/src/models/trabajador_model.dart';

class TrabajadoresServices{
  final String _url = 'https://wynservices-40500.firebaseio.com';

Future<bool> crearTrabajador(TrabajadorModel trabajador) async{
  final url ='$_url/trabajadores.json';
  final resp= await http.post(url, body: trabajadorModelToJson(trabajador));
  final decodedData = json.decode(resp.body);
  print(decodedData);
  return true;
  }
  Future<bool> modificarTrabajador(TrabajadorModel trabajador) async{
  final url ='$_url/trabajadores/${trabajador.id}.json';
  final resp= await http.put(url, body: trabajadorModelToJson(trabajador));
  final decodedData = json.decode(resp.body);
  print(decodedData);
  return true;
  }
  Future<List<TrabajadorModel>> cargarTrabajadores()async{
  final url ='$_url/trabajadores.json';
  final resp= await http.get(url);
  final Map<String,dynamic> decodedData = json.decode(resp.body);
  final List<TrabajadorModel> trabajadores = new List();
  if (decodedData == null) {
    return [];
  }
  decodedData.forEach((id,traba){
    final trabaTemp=TrabajadorModel.fromJson(traba);
    trabaTemp.id=id;
    trabajadores.add(trabaTemp);
  });
  // print(trabajadores);
  return trabajadores;
  }
  Future<bool> borrarTrabajador(String id) async{
    final url ='$_url/trabajadores/$id.json';
    final resp= await http.delete(url);
     print(json.decode(resp.body));
    return true;  
    }
}