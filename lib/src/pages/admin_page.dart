import 'package:flutter/material.dart';
import 'package:wynservices/src/models/trabajador_model.dart';
import 'package:wynservices/src/services/trabajadores_services.dart';

class AdminPage extends StatelessWidget {
  final trabajadoresServices = new TrabajadoresServices();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text('Admin'),
      ),
      body: _crearListadoDeTrabajadores(),
      floatingActionButton: _crearBoton(context),
    );
  }

  Widget _crearListadoDeTrabajadores() {
    return FutureBuilder(
      future: trabajadoresServices.cargarTrabajadores(),
      builder: (BuildContext context,
          AsyncSnapshot<List<TrabajadorModel>> snapshot) {
        if (snapshot.hasData) {
          final trabajadores = snapshot.data;
          return ListView.builder(
              itemCount: trabajadores.length,
              itemBuilder: (context, i) => _crearItem(context,trabajadores[i]));
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, TrabajadorModel trabajador) {
    return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (direccion){
        trabajadoresServices.borrarTrabajador(trabajador.id);
      },
          child: ListTile(
        title: Text('${trabajador.nombre} - ${trabajador.ocupacion}'),
        subtitle: Text(trabajador.id),
        onTap: ()=>Navigator.pushNamed(context, 'registro', arguments: trabajador),
      ),
    );
  }

  _crearBoton(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      backgroundColor: Colors.deepOrange,
      onPressed: () => Navigator.pushNamed(context, 'registro'),
    );
  }
}
