import 'dart:io';

import 'package:flutter/material.dart';
import 'package:wynservices/src/models/trabajador_model.dart';
import 'package:wynservices/src/services/trabajadores_services.dart';
import 'package:wynservices/src/utils/utils.dart' as utils;
import 'package:image_picker/image_picker.dart';

class RegistroPage extends StatefulWidget {
  @override
  _RegistroPageState createState() => _RegistroPageState();
}

class _RegistroPageState extends State<RegistroPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final trabajadoresServices = new TrabajadoresServices();

  bool _guardando = false;
  TrabajadorModel trabajador = new TrabajadorModel();
  File foto;

  @override
  Widget build(BuildContext context) {
    final TrabajadorModel prodData = ModalRoute.of(context).settings.arguments;
    if (prodData != null) {
      trabajador = prodData;
    }
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text('Registro'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.photo_size_select_actual),
            onPressed: _seleccionarFoto,
          ),
          IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: _tomarFoto,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                _mostrarfoto(),
                _crearNombre(),
                // _crearFechaDeNacimiento(),
                _crearDireccionDomicilio(),
                _crearDireccionLocalComercial(),
                _crearNumeroDeCedula(),
                // _crearInstruccion(),
                _crearOcupacion(),
                _crearExperienciaLaboral(),
                _crearNumerodeContacto(),
                _crearDisponible(),
                _crearBoton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: trabajador.nombre,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nombre :'),
      onSaved: (value) => trabajador.nombre = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese su nombre completo';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearDireccionDomicilio() {
    return TextFormField(
      initialValue: trabajador.direcciondomicilio,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Dirección de domicilio :'),
      onSaved: (value) => trabajador.direcciondomicilio = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese su nombre completo';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearDireccionLocalComercial() {
    return TextFormField(
      initialValue: trabajador.direccionlocal,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Dirección de local comercial :'),
      onSaved: (value) => trabajador.direccionlocal = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese su nombre completo';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearNumeroDeCedula() {
    return TextFormField(
      initialValue: trabajador.numerocedula.toString(),
      keyboardType: TextInputType.number,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Número de cédula :'),
      onSaved: (value) => trabajador.numerocedula = int.parse(value),
      validator: (value) {
        if (utils.esNumero(value)) {
          return null;
        } else {
          return 'Sólo se permite números';
        }
      },
    );
  }

  Widget _crearOcupacion() {
    return TextFormField(
      initialValue: trabajador.ocupacion,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Ocupación :'),
      onSaved: (value) => trabajador.ocupacion = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese su nombre completo';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearExperienciaLaboral() {
    return TextFormField(
      initialValue: trabajador.experiencialaboral,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Experiencia laboral :'),
      onSaved: (value) => trabajador.experiencialaboral = value,
      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese su nombre completo';
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearNumerodeContacto() {
    return TextFormField(
      initialValue: trabajador.numerodecontacto.toString(),
      keyboardType: TextInputType.number,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Número de contacto :'),
      onSaved: (value) => trabajador.numerodecontacto = int.parse(value),
      validator: (value) {
        if (utils.esNumero(value)) {
          return null;
        } else {
          return 'Sólo se permite números';
        }
      },
    );
  }

  Widget _crearDisponible() {
    return SwitchListTile(
      value: trabajador.disponible,
      title: Text('Disponible'),
      activeColor: Colors.deepOrange,
      onChanged: ((value) => setState(() {
            trabajador.disponible = value;
          })),
    );
  }

  Widget _crearBoton() {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      color: Colors.deepOrange,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      onPressed: (_guardando) ? null : _submit,
    );
  }

  void _submit() {
    if (formKey.currentState.validate()) {
      //Cuando el formulario es valido
      print('[Registro] OK');
      formKey.currentState.save();
      setState(() {
        _guardando = true;
      });
      // print(trabajador.nombre);
      // print(trabajador.direcciondomicilio);
      // print(trabajador.direccionlocal);
      // print(trabajador.numerocedula);
      // print(trabajador.ocupacion);
      // print(trabajador.experiencialaboral);
      // print(trabajador.numerodecontacto);
      if (trabajador.id == null) {
        trabajadoresServices.crearTrabajador(trabajador);
      } else {
        trabajadoresServices.modificarTrabajador(trabajador);
      }
      //Descomentar si se quiere guardar varias veces
      // setState(() {_guardando = false;});

      mostrarSnackbar('Registro guardado');
      Navigator.pop(context);
    } else {
      //Cuando no es formulario es valido
      print('[Registro] ERROR');
    }
  }

  void mostrarSnackbar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(milliseconds: 1500),
    );
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  Widget _mostrarfoto() {
    if (trabajador.fotoUrl != null) {
      // TODO: tengo que hacer esto
      return FadeInImage(
        image: NetworkImage(trabajador.fotoUrl),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        width: double.infinity,
        fit: BoxFit.cover,
      );
    } else {
      return Image(
        image: AssetImage(foto?.path ?? 'assets/no-image.png'),
        height: 300.0,
        fit: BoxFit.cover,
      );
    }
  }

  _seleccionarFoto() async {
    foto = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (foto != null) {
     trabajador.fotoUrl=null;
    }
    setState(() {});
  }

  _tomarFoto() async {
    foto = await ImagePicker.pickImage(source: ImageSource.camera);
    if (foto != null) {
      //Limpieza
    }
    setState(() {});
  }
}
