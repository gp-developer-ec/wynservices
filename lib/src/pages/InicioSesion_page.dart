import 'package:flutter/material.dart';

class InicioSecionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
          body: Stack(
        children: <Widget>[
          _crearFondo(context),
        ],
      )),
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondo = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(198, 75, 12, 1),
        Color.fromRGBO(218, 147, 69, 1),
      ])),
    );
    final circulom = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(112, 13, 104, 0.05),
      ),
    );
    final circulov = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(117, 169, 58, 0.05),
      ),
    );
    final circuloa = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(42, 65, 143, 0.05),
      ),
    );
    final logo = Container(
      padding: EdgeInsets.only(top: 80.0),
      child: Column(
        children: <Widget>[
          Image(
            image: AssetImage('assets/logo.png'),
            height: 150.0,
          ),
          SizedBox(height: 10.0,width: double.infinity,),
          // Text('Iniciar Sesión',style: TextStyle(color: Colors.white,fontSize: 25.0),),
        ],
      ),
    );
    return Stack(
      children: <Widget>[
        fondo,
        Positioned(
          child: circuloa,
          top: 90.0,
          left: 30.0,
        ),
        Positioned(
          child: circulov,
          top: 20.0,
          right: -5.0,
        ),
        Positioned(
          child: circulom,
          top: 200.0,
          right: 150.0,
        ),
        logo,
      ],
    );
  }
}
