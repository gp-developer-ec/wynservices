// To parse this JSON data, do
//
//     final trabajdor = trabajdorFromJson(jsonString);

import 'dart:convert';

TrabajadorModel trabajdorFromJson(String str) => TrabajadorModel.fromJson(json.decode(str));

String trabajadorModelToJson(TrabajadorModel data) => json.encode(data.toJson());

class TrabajadorModel {
    String id;
    String nombre;
    String fdn;
    String direcciondomicilio;
    String direccionlocal;
    int numerocedula;
    String instruccion;
    String ocupacion;
    String experiencialaboral;
    int numerodecontacto;
    bool disponible;
    String fotoUrl;

    TrabajadorModel({
        this.id,
        this.nombre,
        this.fdn,
        this.direcciondomicilio,
        this.direccionlocal,
        this.numerocedula,
        this.instruccion,
        this.ocupacion,
        this.experiencialaboral,
        this.numerodecontacto,
        this.disponible=true,
        this.fotoUrl,
    });

    factory TrabajadorModel.fromJson(Map<String, dynamic> json) => TrabajadorModel(
        id: json["id"],
        nombre: json["nombre"],
        fdn: json["fdn"],
        direcciondomicilio: json["direcciondomicilio"],
        direccionlocal: json["direccionlocal"],
        numerocedula: json["numerocedula"],
        instruccion: json["instruccion"],
        ocupacion: json["ocupacion"],
        experiencialaboral: json["experiencialaboral"],
        numerodecontacto: json["numerodecontacto"],
        disponible: json["disponible"],
        fotoUrl: json["fotoUrl"],
    );

    Map<String, dynamic> toJson() => {
        // "id": id,
        "nombre": nombre,
        "fdn": fdn,
        "direcciondomicilio": direcciondomicilio,
        "direccionlocal": direccionlocal,
        "numerocedula": numerocedula,
        "instruccion": instruccion,
        "ocupacion": ocupacion,
        "experiencialaboral": experiencialaboral,
        "numerodecontacto": numerodecontacto,
        "disponible": disponible,
        "fotoUrl": fotoUrl,
    };
}
